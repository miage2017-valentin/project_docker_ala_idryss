# Lancement du projet

Veuillez d'abord vous assurer que vous disposez de Docker installé et prêt sur votre machine.

Ensuite, déplacez-vous à la racine du projet puis ouvrez un terminal. Exécutez la commande suivante :

```
docker-compose up
```

Cette commande va lancer toute la stack Docker. Elle va faire appel aux différents dockerfiles dans les répertoires composants la stack (front_end, back_end).

back_end dockerfile (image back-end-audiostore_1) :

* Tire l'image node:10.15.0
* Sélectionne le working directory
* Copie package.json vers le dossier ./ au sein du conteneur
* Copie la racine vers la racine au sein du conteneur
* Exécute npm install
* Informe docker que le conteneur écoute le port 3001
* Ajoute le script wait dans le répertoire /wait à la racine du conteneur (permet de lancer le web service qui va remplir la base de données après que celle-ci soit disponible)
* Exécute le script wait et npm start

front_end dockerfile (image front-end-audiostore_1) :

* Tire l'image node:10.15.0
* Créer un repertoire /usr/src/app et s'en sert comme working directory
* Ajoute /usr/src/app/node_modules/.bin au PATH
* Copie package.json vers le working directory
* Exécute npm install et npm install -g @angular/cli@1.7.1
* Copie la racine vers la racine au sein du conteneur
* Informe Docker que le conteneur écoute le port 4200
* Exécute le script ng serve