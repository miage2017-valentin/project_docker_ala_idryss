import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { PluginAudio } from './plugin-audio';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class PluginAudioService {
  constructor(private http: HttpClient, private user: UserService) {
  }

  createPlugin(json: any): PluginAudio {
    let plugin: PluginAudio = json as PluginAudio;
    plugin.image = environment.urlAPI + json.image;
    plugin.thumbnail = environment.urlAPI + json.thumbnail;
    return plugin;
  }

  getPlugins(params = {}): Promise<PluginAudio[]> {
    console.log(environment.urlAPI + "/plugins");
    return this.http.get(environment.urlAPI + "/plugins", { params }).toPromise().then((response: any[]) => {
      return new Promise<PluginAudio[]>((resolve, reject) => {
        let plugins: PluginAudio[] = [];
        for (let plugin of response) {
          plugins.push(this.createPlugin(plugin));
        }
        resolve(plugins);
      })
    })
  }

  getPluginById(id: String): Promise<PluginAudio> {
    console.log(environment.urlAPI + "/plugin/" + id);
    return this.http.get(environment.urlAPI + "/plugin/" + id).toPromise().then((response: any) => {
      return new Promise<PluginAudio>((resolve, reject) => {
        if (response !== null)
          resolve(this.createPlugin(response));
        else
          resolve(undefined);
      })
    })
  }

  countPlugins(params = {}): Promise<number> {
    console.log(environment.urlAPI + "/plugins");
    return this.http.get(environment.urlAPI + "/plugins/count", { params }).toPromise().then((response: any) => {
      return new Promise<number>((resolve, reject) => {
        resolve(response.count);
      })
    })
  }

  addPlugin(plugin: PluginAudio, image: File): Promise<PluginAudio> {
    const headers = new HttpHeaders({'Authorization':'Bearer ' + this.user.token});
    let formData = new FormData();
    formData.append('image', image);
    formData.append('plugin', JSON.stringify(plugin));
    return this.http.post(environment.urlAPI + "/plugin", formData, {headers}).toPromise().then((response: any) => {
      return new Promise<PluginAudio>((resolve, reject) => {
        resolve(response.plugin);
      })
    })
  }

  deletePluginById(id: String): Promise<any> {
    const headers = new HttpHeaders({'Authorization':'Bearer ' + this.user.token});
    console.log(environment.urlAPI + "/plugin/" + id);
    return this.http.delete(environment.urlAPI + "/plugin/" + id, {headers}).toPromise();
  }

  updatePlugin(plugin: PluginAudio, image: File): Promise<PluginAudio> {
    const headers = new HttpHeaders({'Authorization':'Bearer ' + this.user.token});
    let formData = new FormData();
    formData.append('image', image);
    formData.append('plugin', JSON.stringify(plugin));
    return this.http.put(environment.urlAPI + "/plugin/" + plugin._id, formData, {headers}).toPromise().then((response: any) => {
      return new Promise<PluginAudio>((resolve, reject) => {
        resolve(response.plugin);
      })
    })
  }
}
