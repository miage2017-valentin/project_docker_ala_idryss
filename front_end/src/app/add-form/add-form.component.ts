import { Component, OnInit } from '@angular/core';
import { PluginAudioService } from '../plugin-audio.service';
import { Router } from "@angular/router"
import { UserService } from '../user.service';

@Component({
  selector: 'app-add-form',
  templateUrl: './add-form.component.html',
  styleUrls: ['./add-form.component.css'],
})
export class FormComponent implements OnInit {

  constructor(private pluginAudioService: PluginAudioService,
    private router: Router,
    private userService: UserService) {
    if (!this.userService.isConnected) {
      this.router.navigate(['/home'])
    }
  }
  ngOnInit() {

  }

  onNoClick(): void {
  }
  addPlugin(result) {
    this.pluginAudioService.addPlugin(result.plugin, result.file).then((plugin) => {
      this.router.navigate(['/plugins/details', plugin._id])
    })
  }
}
