import { Component, OnInit } from '@angular/core';
import { PluginAudioService } from '../plugin-audio.service';
import { PluginAudio } from '../plugin-audio';
import { environment } from '../../environments/environment';
import { CardPluginComponent } from '../card-plugin/card-plugin.component';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  public plugins: PluginAudio[];
  constructor(private pluginAudio: PluginAudioService) { }

  ngOnInit() {
    this.pluginAudio.getPlugins({perPage: environment.perPageHome}).then((plugins : PluginAudio[])=>{
      this.plugins = plugins;
    })
  }
}
