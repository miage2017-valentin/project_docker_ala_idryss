import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTableModule} from '@angular/material/table';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatChipsModule} from '@angular/material/chips';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PluginDetailsComponent } from './plugin-details/plugin-details.component';
import { ShopComponent } from './shop/shop.component';
import { LoginModalComponent } from './login-modal/login-modal.component'
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CardPluginComponent } from './card-plugin/card-plugin.component';
import { FormComponent } from './add-form/add-form.component';
import { SignUpComponent } from './sign-form/sign-form.component';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { FormPluginComponent } from './form-plugin/form-plugin.component';
import { UpdateFormComponent } from './update-form/update-form.component';


const appRoutes: Routes = [
  { path: 'plugins/add', component: FormComponent},
  { path: 'plugins/details/:id', component: PluginDetailsComponent },
  { path: 'plugins/update/:id', component: UpdateFormComponent },
  { path: 'shop', component: ShopComponent},
  { path: 'home', component: MainComponent },
  { path: '', redirectTo: "/home",  pathMatch: 'full'},
  { path: '**', component: PageNotFoundComponent },
  { path: 'add-form', component: FormComponent},
  { path: 'sign-form', component: SignUpComponent}


];


@NgModule({
  entryComponents:[
    LoginModalComponent
  ],
  declarations: [
    AppComponent,
    MainComponent,
    PluginDetailsComponent,
    ShopComponent,
    LoginModalComponent,
    PageNotFoundComponent,
    CardPluginComponent,
    FormComponent,
    SignUpComponent,
    FormPluginComponent,
    UpdateFormComponent
  ],
  imports: [
    MatSnackBarModule,
    NgxWebstorageModule.forRoot(),
    MatChipsModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatInputModule,
    FormsModule,
    BrowserAnimationsModule,
    BrowserModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    HttpClientModule,
    FlexLayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
