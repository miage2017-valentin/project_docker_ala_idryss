import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import {UserService} from '../user.service';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.css']
})
export class LoginModalComponent implements OnInit {

  public login: String;
  public password: String;
  
  constructor(
    public dialogRef: MatDialogRef<LoginModalComponent>, private user: UserService, public snackBar: MatSnackBar) {}

  ngOnInit(){
    
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  clickLogin(){
    this.user.login(this.login, this.password).then(()=>{
      this.dialogRef.close();
    })
    .catch((message)=>{
      let config = new MatSnackBarConfig();
      config.panelClass = 'center-snackbar';
      config.duration = 1000;
      this.snackBar.open(message, '', config);
    })
  }
}
