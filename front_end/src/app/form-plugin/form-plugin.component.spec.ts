import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormPluginComponent } from './form-plugin.component';

describe('FormPluginComponent', () => {
  let component: FormPluginComponent;
  let fixture: ComponentFixture<FormPluginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormPluginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormPluginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
