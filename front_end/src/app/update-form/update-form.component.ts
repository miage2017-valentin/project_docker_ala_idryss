import { Component, OnInit } from '@angular/core';
import { PluginAudioService } from '../plugin-audio.service';
import { ActivatedRoute, Router } from '@angular/router';
import {Location} from '@angular/common';
import { PluginAudio } from '../plugin-audio';
import { UserService } from '../user.service';

@Component({
  selector: 'app-update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.css']
})
export class UpdateFormComponent implements OnInit {
  public plugin: PluginAudio = {} as PluginAudio;
  constructor(private pluginAudioService: PluginAudioService,
    private router: Router,
    private userService: UserService,
    private route: ActivatedRoute, private _location: Location) {
      if (!this.userService.isConnected) {
        this.router.navigate(['/home'])
      }
    }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');
    this.pluginAudioService.getPluginById(id)
    .then((plugin)=>{
      console.log(plugin);
      this.plugin = plugin;
    });
  }

  updatePlugin(result){
    this.pluginAudioService.updatePlugin(result.plugin, result.file).then((plugin) => {
      this.router.navigate(['/plugins/details', plugin._id])
    })
  }

  clickBack(): void {
    this._location.back(); 
  }

}
