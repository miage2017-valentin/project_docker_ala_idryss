import { Injectable } from '@angular/core';
import {LocalStorageService, SessionStorageService} from 'ngx-webstorage';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public token : string;
  private keyOfTokenInStorage : string = "token";
  public actualUser : User = {} as User;
  public isConnected : boolean = false;

  constructor(private http: HttpClient, private storage:LocalStorageService) {
    this.token = this.storage.retrieve(this.keyOfTokenInStorage);
    if(this.token !== null){
      console.log("Check token : "+ this.token);
      this.checkToken();
    }
  }

  login(login, password){
    return new Promise((resolve, reject)=>{
      this.http.post(environment.urlAPI + "/login", { login, password }).toPromise().then((response: any) => {
        if(response.auth){
          this.token = response.token
          this.storage.store(this.keyOfTokenInStorage, this.token);
          this.isConnected = true;
          this.profile().then((response: any)=>{
            this.actualUser = response.user;
            resolve();
          })
          .catch(()=>{
            this.logout();
          });
        }
      })
      .catch((response : any)=>{
        this.isConnected = false;
        reject(response.error.message);
      })
    });
  }

  register(login, name, passwordConfirm, password, url){
    return new Promise((resolve, reject)=>{
     this.http.post(environment.urlAPI + "/register", { login, name, passwordConfirm, password, url }).toPromise().then((response: any) => {
        if(response.auth){
          passwordConfirm != password
          this.token = response.token
          this.storage.store(this.keyOfTokenInStorage, this.token);
          this.isConnected = true;
          this.profile().then((response: any)=>{
            this.actualUser = response.user;
            resolve();
          })
          .catch(()=>{
            this.logout();
          });
        }
      })
      .catch((response : any)=>{
        this.isConnected = false;
        reject(response.error.message);
      })
    });
  }

  checkToken(){
    let token = this.token;
    return new Promise((resolve, reject)=>{
      this.http.get(environment.urlAPI + "/checkToken", { params : {token} }).toPromise().then((response: any) => {
        if(response.valid){
          this.actualUser = response.user;
          this.isConnected = true;
          resolve();
        }
      })
      .catch((response : any)=>{
        this.logout();
        reject(response.error.message);
      })
    });
  }

  profile(){
    const headers = new HttpHeaders({'Authorization':'Bearer ' + this.token});
    return this.http.get(environment.urlAPI + "/profile", {headers}).toPromise();
  }

  logout(){
    this.storage.clear(this.keyOfTokenInStorage);
    this.actualUser = {} as User;
    this.isConnected = false;
  }
}
