import { Component, OnInit } from '@angular/core';
import { PluginAudio } from '../plugin-audio';
import { environment } from '../../environments/environment';
import { PluginAudioService } from '../plugin-audio.service';
import * as _ from 'lodash';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {
  public plugins: PluginAudio[];
  public searchValue: String;
  public perPage;
  public loading: Boolean;
  public debounceSearch = _.debounce(() => this.search(), 300, true);
  public categoriesSearch: Set<String>;
  public nbPlugins: number;

  constructor(private pluginAudio: PluginAudioService, public snackBar: MatSnackBar) {
    this.perPage = environment.perPageShop;
    this.categoriesSearch = new Set();
    this.searchValue  = "";
  }

  ngOnInit() {
    this.search();
  }

  search(loading: boolean = true) {
    this.loading = (loading) ? this.loading = true : this.loading = false;
    console.log(this.searchValue);
    const arrayOfCategories = Array.from(this.categoriesSearch)
    this.pluginAudio.getPlugins({ perPage: this.perPage, text: this.searchValue, categories: arrayOfCategories })
      .then((plugins: PluginAudio[]) => {
        this.pluginAudio.countPlugins({ perPage: this.perPage, text: this.searchValue, categories: arrayOfCategories })
          .then((count) => {
            this.nbPlugins = count;
            console.log(this.nbPlugins)
            this.loading = (loading) ? this.loading = false : this.loading = false;
            this.plugins = plugins;
          })
      });
  }

  searchByCategory(category) {
    let arrayOfCategories = Array.from(this.categoriesSearch);
    if (arrayOfCategories.indexOf(category) === -1) {
      this.loading = true;
      this.categoriesSearch.add(category);
      this.search();
    }
  }

  removeCategory(category) {
    this.categoriesSearch.delete(category);
    this.search()
  }

  morePlugins() {
    this.perPage += environment.perPageShop;
    this.search(false);
  }

  clickDeleteFinish(success){
    if(success){
      let config = new MatSnackBarConfig();
      config.panelClass = 'center-snackbar';
      config.duration = 3000;
      if(success){
        this.snackBar.open("Plugin audio deleted with success", '', config);
      }
      this.search();
    }
  }
}
