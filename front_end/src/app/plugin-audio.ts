import { Vendor } from './vendor';

export interface PluginAudio {
    _id: String,
    creatorName: String,
    createdAt: Date,
    description: String,
    image: string,
    shortDescription: String,
    thumbnail: String,
    vendor: Vendor,
    categories: string[],
    controls: any[]
}
