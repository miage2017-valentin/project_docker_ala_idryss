import { Component } from '@angular/core';
import { LoginModalComponent } from './login-modal/login-modal.component';
import { FormComponent } from './add-form/add-form.component';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { SignUpComponent } from './sign-form/sign-form.component';
import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(public dialog: MatDialog, public user: UserService) {
  }

  openDialogLogIn(): void {
    const dialogRef = this.dialog.open(LoginModalComponent, {
      width: '250px'
    });


    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result);
      //this.animal = result;
    });
  }

  openAddForm(): void {
    const dialogForm = this.dialog.open(FormComponent, {});
  }

  openSignUpForm(): void {
    const dialogForm = this.dialog.open(SignUpComponent, {
      width: '500px'
    });
  }

  logout() {
    this.user.logout();
  }
}
