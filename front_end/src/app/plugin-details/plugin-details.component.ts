import { Component, OnInit } from '@angular/core';
import { PluginAudioService } from '../plugin-audio.service';
import { ActivatedRoute, Router } from '@angular/router';
import {Location} from '@angular/common';
import { PluginAudio } from '../plugin-audio';
import { UserService } from '../user.service';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material';

@Component({
  selector: 'app-plugin-details',
  templateUrl: './plugin-details.component.html',
  styleUrls: ['./plugin-details.component.css']
})
export class PluginDetailsComponent implements OnInit {
  public plugin: PluginAudio = {vendor:{}} as PluginAudio;
  public displayedColumns : String[];
  constructor(private pluginAudioService: PluginAudioService,
    private router: Router,
    private route: ActivatedRoute, private _location: Location,
    private user: UserService, public snackBar: MatSnackBar) {
      this.displayedColumns = ['name', 'default', 'min', 'max'];
    }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');
    this.pluginAudioService.getPluginById(id)
    .then((plugin)=>{
      console.log(plugin);
      if(plugin === undefined){
        this.router.navigate(['/home'])
      }
      this.plugin = plugin;
    });
  }

  clickBack(): void {
    this._location.back(); 
  }

  deletePlugin(plugin){
    this.pluginAudioService.deletePluginById(plugin._id).then((response) => {
      console.log(response.message);
      this.router.navigate(['/shop']);
    })
    .catch((response)=>{
      let config = new MatSnackBarConfig();
      config.panelClass = 'center-snackbar';
      config.duration = 1000;
      this.snackBar.open(response.message, '', config);
    })
  }
}
