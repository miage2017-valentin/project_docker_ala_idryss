export const environment = {
  production: true,
  urlAPI: 'http://localhost:3001',
  perPageHome: 5,
  perPageShop: 10
};
