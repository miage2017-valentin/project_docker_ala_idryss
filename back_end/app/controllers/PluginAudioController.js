const models = require("../models");
const sharp = require('sharp');
const urlImageEmpty = '/public/images/image-empty.png';
module.exports = class {

    getListPluginAudio(req, res) {
        const start = (req.query.start) ? parseInt(req.query.start) : 0;
        const limit = (req.query.perPage) ? parseInt(req.query.perPage) : 10;
        const text = (req.query.text) ? req.query.text : null;
        const categories = (req.query.categories) ? req.query.categories : null;
        console.log(start + " -- " + limit + " -- " + text);

        let condition = {};
        if (text !== null) {
            // condition.$text = { $search: text };
            condition.$or = [
                { creatorName: { "$regex": text, "$options": "i" } },
                { description: { "$regex": text, "$options": "i" } },
                { shortDescription: { "$regex": text, "$options": "i" } }
            ]
        }
        if (categories !== null) {
            condition.categories = { $all: categories };
        }
        models.PluginAudio.find(condition)
            .populate('vendor', '-password -login')
            .skip(start)
            .limit(limit)
            .exec((err, plugins) => {
                console.log(err);
                return res.json(plugins);
            });
    }

    getDetailsPlugin(req, res) {
        models.PluginAudio.findById(req.params.id)
            .populate('controls')
            .populate('vendor', '-password -login')
            .exec((err, plugin) => {
                return res.json(plugin);
            });
    }

    getPluginOfCurrentUser(req, res) {
        models.PluginAudio.find({ vendor: req.currentUser._id })
            .populate('controls')
            .exec((err, plugin) => {
                return res.json(plugin);
            });
    }

    count(req, res) {
        const text = (req.query.text) ? req.query.text : null;
        const categories = (req.query.categories) ? req.query.categories : null;
        let condition = {};
        if (text !== null) {
            condition.$or = [
                { creatorName: { "$regex": text, "$options": "i" } },
                { description: { "$regex": text, "$options": "i" } },
                { shortDescription: { "$regex": text, "$options": "i" } }
            ]
        }
        if (categories !== null) {
            condition.categories = { $all: categories };
        }
        models.PluginAudio.countDocuments(condition)
            .exec((err, count) => {
                console.log(err);
                return res.status(200).send({ count: count });
            });
    }

    async create(req, res) {
        let file = req.file;
        let pluginAudio = req.body.plugin;
        if (pluginAudio) {
            pluginAudio = JSON.parse(pluginAudio);
            let objPluginAudio = new models.PluginAudio(pluginAudio);
            objPluginAudio.createdAt = new Date();
            objPluginAudio.vendor = req.currentUser;
            objPluginAudio.controls = [];
            if (file) {
                console.log(file);
                console.log(file.path);
                let path = file.path.replace(/\\/g, '/');
                objPluginAudio.image = '/' + path;

                let pathThumbnail = file.destination + 'thumbnails/' + file.filename + "-600x450.png";
                objPluginAudio.thumbnail = '/' + pathThumbnail;
                await sharp(file.destination + file.filename)
                    .resize(600, 450, {
                        background: { r: 0, g: 0, b: 0, alpha: 0 },
                        fit: "contain"
                    })
                    // .background()
                    // .embed()
                    .toFile(pathThumbnail, (err, info) => {
                        if (err != null) {
                            res.status(400).send({ message: "Plugin not created : Error with upload image" });
                        }
                    });
            } else {
                objPluginAudio.image = urlImageEmpty;
                objPluginAudio.thumbnail = urlImageEmpty;
            }
            objPluginAudio.save((err) => {
                console.log(err);
                if (err == null) {
                    for (let control of pluginAudio.controls) {
                        let controlObj = new models.Control(control);
                        controlObj.pluginAudio = objPluginAudio._id;
                        controlObj.save();
                        objPluginAudio.controls.push(controlObj);
                    }
                    objPluginAudio.save().then(() => {
                        res.status(201).send({ message: "Plugin created", plugin: objPluginAudio });
                    });
                } else {
                    res.status(400).send({ message: "Plugin not created" });
                }
            });
        } else {
            res.status(400).send({ message: "Plugin not entered" });
        }
    }

    delete(req, res) {
        let idPlugin = req.params.id;
        if (idPlugin != undefined) {
            models.PluginAudio.findById(idPlugin)
                .populate('vendor', '-password -login')
                .exec((err, plugin) => {
                    if (plugin.vendor._id.toString() === req.currentUser._id.toString()) {
                        models.PluginAudio.deleteOne({ _id: idPlugin }).then((err) => {
                            if (err.ok === 1) {
                                res.status(200).send({ message: "Plugin audio deleted" });
                            } else {
                                res.status(500).send({ message: err });
                            }
                        })
                    } else {
                        res.status(403).send({ message: "You can't delete this plugin" });
                    }
                });
        } else {
            res.status(400).send({ message: "Id Plugin not entered" });
        }
    }

    update(req, res) {
        let idPlugin = req.params.id;
        let file = req.file;
        let pluginAudio = req.body.plugin;
        if (pluginAudio && idPlugin) {
            //Check if plugin exist
            models.PluginAudio.findById(idPlugin)
                .exec(async (err, pluginFound) => {
                    if (err == null) {
                        // Keep image/thumbnail path of older object
                        let imageBackup = pluginFound.image;
                        let thumbnailBackup = pluginFound.thumbnail;

                        pluginAudio = JSON.parse(pluginAudio);
                        let objPluginAudio = Object.assign(pluginFound, pluginAudio);
                        // Field not updated
                        objPluginAudio.image = imageBackup;
                        objPluginAudio.thumbnail = thumbnailBackup;
                        objPluginAudio.vendor = req.currentUser;
                        objPluginAudio.controls = [];

                        // Clean all controls for this object
                        await models.Control.deleteMany({ pluginAudio: pluginFound });
                        if (file) {
                            let path = file.path.replace(/\\/g, '/');
                            objPluginAudio.image = '/' + path;

                            let pathThumbnail = file.destination + 'thumbnails/' + file.filename + "-600x450.png";
                            objPluginAudio.thumbnail = '/' + pathThumbnail;
                            await sharp(file.destination + file.filename)
                                .resize(600, 450, {
                                    background: { r: 0, g: 0, b: 0, alpha: 0 },
                                    fit: "contain"
                                })
                                .toFile(pathThumbnail, (err, info) => {
                                    if (err != null) {
                                        res.status(400).send({ message: "Plugin not created : Error with upload image" });
                                    }
                                });
                        }
                        objPluginAudio.save((err) => {
                            if (err == null) {
                                for (let control of pluginAudio.controls) {
                                    let controlObj = new models.Control(control);
                                    controlObj.pluginAudio = objPluginAudio._id;
                                    controlObj.save();
                                    objPluginAudio.controls.push(controlObj);
                                }
                                objPluginAudio.save().then(() => {
                                    res.status(200).send({ message: "Plugin updated", plugin: objPluginAudio });
                                });
                            } else {
                                res.status(400).send({ message: "Plugin not updated" });
                            }
                        });
                    } else {
                        res.status(404).send({ message: "Plugin not found" });
                    }
                });
        } else {
            res.status(400).send({ message: "Plugin not entered" });
        }
    }
}