const models = require('./index.js');
const pluginAudioJson = require('./pluginAudio.json');
const bcrypt = require('bcrypt');

module.exports = {
    clear: () => {
        let listPromise = [];
        Object.keys(models).forEach((key, index) => {
            listPromise.push(models[key].deleteMany({}));
        });

        return Promise.all(listPromise);
    },
    save: () => {
        let listPluginAudioObj = [];
        let listPromisePluginAudio = [];
        //Create plugin audio
        for (let pluginAudio of pluginAudioJson.pluginsAudio) {
            let objPluginAudio = new models.PluginAudio(pluginAudio);
            listPromisePluginAudio.push(new Promise((resolve, reject) => {
                console.log("Plugin audio : create " + objPluginAudio.creatorName);
                objPluginAudio.createdAt = new Date();
                objPluginAudio.save((err) => {
                    if (err) {
                        console.log(err);
                    } else {
                        //create Controls
                        for (let control of pluginAudio.controlsRef) {
                            let controlObj = new models.Control(control);
                            controlObj.pluginAudio = objPluginAudio._id;
                            controlObj.save();
                            objPluginAudio.controls.push(controlObj);
                        }
                        objPluginAudio.save().then(() => {
                            resolve();
                        });
                    }
                });
            }));
            listPluginAudioObj.push(objPluginAudio);
        }
        Promise.all(listPromisePluginAudio).then(() => {
            //Create vendor
            let listPromiseVendor = [];
            for (let vendor of pluginAudioJson.vendors) {
                let vendorObj = new models.Vendor(vendor);
                listPromiseVendor.push(new Promise(async (resolve, reject) => {
                    vendorObj.password = await bcrypt.hash(vendorObj.password, parseInt(process.env.SALTROUNDS));
                    vendorObj.save().then(() => {
                        for (let indexPlugin of vendor.pluginsAudioRef) {
                            vendorObj.pluginsAudio.push(listPluginAudioObj[indexPlugin]);
                            listPluginAudioObj[indexPlugin].vendor = vendorObj._id;
                            listPluginAudioObj[indexPlugin].save();
                        }
                        vendorObj.save().then(() => { resolve(); });
                    })
                }))
            }
            return Promise.all(listPromiseVendor);
        }).then(() => {
            for (let pedalBoard of pluginAudioJson.pedalsBoard) {
                let pedalBoardObj = new models.PedalBoard(pedalBoard);
                pedalBoardObj.save().then(() => {
                    for (let indexPlugin of pedalBoard.pluginsAudioRef) {
                        pedalBoardObj.pluginsAudio.push(listPluginAudioObj[indexPlugin]);
                        listPluginAudioObj[indexPlugin].pedalsBoard.push(pedalBoardObj);
                        listPluginAudioObj[indexPlugin].save();
                    }
                    pedalBoardObj.save();
                })
            }
        })
    }
}