const mongoose = require('mongoose');

let ControlSchema = new mongoose.Schema({
    name: String,
    default: Number,
    min: Number,
    max: Number,
    pluginAudio: { type: mongoose.Schema.Types.ObjectId, ref: 'PluginAudio' }
});

let PedalBoardSchema = new mongoose.Schema({
    name: String,
    screenshot: String,
    author: String,
    pluginsAudio : [{ type: mongoose.Schema.Types.ObjectId, ref: 'PluginAudio' }]
});

let VendorSchema = new mongoose.Schema({
    login: String,
    password: String,
    name: String,
    url: String,
    pluginsAudio : [{ type: mongoose.Schema.Types.ObjectId, ref: 'PluginAudio' }]
});

let PluginAudioSchema = new mongoose.Schema({
    creatorName: String,
    shortDescription: String,
    description: String,
    image: String,
    thumbnail: String,
    createdAt: Date,
    categories: [String],
    vendor: { type: mongoose.Schema.Types.ObjectId, ref: 'Vendor' },
    controls : [{ type: mongoose.Schema.Types.ObjectId, ref: 'Control' }],
    pedalsBoard : [{ type: mongoose.Schema.Types.ObjectId, ref: 'PedalBoard' }]
});

PluginAudioSchema.indexes({'$**': 'text'});

module.exports = {
    Control : mongoose.model('Control', ControlSchema),
    PluginAudio : mongoose.model('PluginAudio', PluginAudioSchema),
    Vendor: mongoose.model('Vendor', VendorSchema),
    PedalBoard: mongoose.model('PedalBoard', PedalBoardSchema)
};