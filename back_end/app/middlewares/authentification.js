const jwt = require('jsonwebtoken');
const models = require("../models");

module.exports = function (req, res, next) {
    let token = req.headers.authorization;
    if (token) {
        token = token.replace(/^Bearer /, "");
        try {
            const decoded = jwt.verify(token, process.env.SECRET_JWT, { ignoreExpiration: false });
            models.Vendor.findById(decoded.id, "-password")
            .exec((err, user) => {
                if (user !== null) {
                    req.currentUser = user;
                    next();
                } else {
                    return res.status(401).send({ auth: false, message: 'User not exist' });
                }
            })
        } catch (err) {
            console.log(err);
            return res.status(401).send({ auth: false, message: 'No token provided.' });
        }
    } else {
        return res.status(401).send({ auth: false, message: 'No token provided.' });
    }
};